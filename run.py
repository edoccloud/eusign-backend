from aiohttp import web
from main import app

web.run_app(app, host='127.0.0.1', port=8080)
