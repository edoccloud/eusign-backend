import io
from aiohttp import web
from eusign import EUSignCP

async def parse_certificate(request):
    eusign = EUSignCP.EUGetInterface()
    eusign.Initialize()
    data = await request.post()
    cert_bytes = data['cert'].file.read()
    cert = {}
    eusign.ParseCertificateEx(cert_bytes, len(cert_bytes), cert)
    return web.json_response(cert)

async def parse_sign(request):
    eusign = EUSignCP.EUGetInterface()
    eusign.Initialize()

    sign_bytes = bytes()
    reader = await request.multipart()
    field = await reader.next()
    assert field.name == 'sign'

    while True:
        chunk = await field.read_chunk()
        if not chunk:
            break
        sign_bytes += chunk

    signer_info = {}
    cert_bytes = bytes()
    eusign.GetSignerInfo(0, None, sign_bytes, len(sign_bytes), signer_info, cert_bytes)
    return web.json_response(signer_info)

app = web.Application()
app.router.add_post('/eusign/certificate/', parse_certificate)
app.router.add_post('/eusign/sign/', parse_sign)

EUSignCP.EULoad()

